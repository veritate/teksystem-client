@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.pluginSerialization)
    application
    `maven-publish`
}

group = "se.veritate"
version = "1.0-SNAPSHOT"

repositories {
    mavenLocal()
    mavenCentral()

    maven(url = "https://oss.sonatype.org/content/repositories/snapshots") {
        mavenContent { snapshotsOnly() }
    }

    maven(url = "https://s01.oss.sonatype.org/content/repositories/snapshots") {
        mavenContent { snapshotsOnly() }
    }
}

dependencies {
    implementation(libs.jsoup)
    implementation(libs.ktorClient.cio)
    implementation(libs.ktorClient.core)
    implementation(libs.ktorClient.contentNegotiation)
    implementation(libs.ktorClient.loggingJvm)
    implementation(libs.slf4j.api)
    implementation(libs.log4j2.api)
    implementation(libs.log4j2.core)
    implementation(libs.arrow.core)
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

publishing {
    repositories {
        maven {
            url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")
            credentials {
                username = properties["repo.username"].toString()
                password = properties["repo.password"].toString()
            }
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "se.veritate"
            artifactId = "teksystem-client"
            version = "1.0-SNAPSHOT"

            from(components["java"])

            pom {
                name.set(project.name)
                description.set("A client for interacting with Bokios REST API")
                url.set("https://gitlab.com/ludde89l/bokio-client")

                licenses {
                    license {
                        name.set("Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                        distribution.set("repo")
                    }
                }

                developers {
                    developer {
                        name.set("Magnus Lundberg")
                    }
                    developer {
                        name.set("Emil Kantis")
                    }
                }

                scm {
                    connection.set("scm:git:git://gitlab.com/ludde89l/bokio-client.git")
                    developerConnection.set("scm:git:ssh://gitlab.com:ludde89l/bokio-client.git")
                    url.set("https://gitlab.com/ludde89l/bokio-client")
                }
            }
        }
    }
}


kotlin {
    target {
        jvmToolchain(11)
    }
}
application {
    mainClass.set("MainKt")
}