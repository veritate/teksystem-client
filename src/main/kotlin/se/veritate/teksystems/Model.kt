package se.veritate.teksystems

import kotlinx.serialization.Serializable
import java.time.Duration


class ReportedTime(val type: String, val time: Duration) {
    override fun toString(): String {
        return "$type $time"
    }
}

@Serializable
data class Credentials(
    val username: String,
    val password: String,
)