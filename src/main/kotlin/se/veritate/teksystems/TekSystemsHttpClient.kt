package se.veritate.teksystems

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.cookies.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalDate
import java.time.YearMonth
import java.time.temporal.TemporalAdjusters
import java.util.*
import java.util.stream.Collectors

suspend fun createTekSystemsHttpClient(
    credentials: Credentials,
    baseUrl: String = "https://timeandexpense.allegisgroup.com",
    clientConfig: HttpClient.() -> Unit = {},
): TekSystemsHttpClient {
    val client = HttpClient(CIO) {

        defaultRequest {
            url(baseUrl)
        }

        install(HttpCookies)
    }.apply(clientConfig)

    client.login(credentials)

    return TekSystemsHttpClient(
        client
    )
}

private suspend fun HttpClient.login(credentials: Credentials) {
    val body = get("/webtime/Login.aspx").body<String>()
    val formParameters = Parameters.build {
        append("txtPassword", credentials.password)
        append("txtUserName", credentials.username)
        append("lBtnLogin", "Inloggning")
        val inputs = Jsoup.parse(body)
            .select("input")
            .select("[type=hidden]")
        for(input in inputs) {
            append(input.attr("name"), input.`val`())
        }
    }
    val response: HttpResponse = submitForm(
        url = "/webtime/Login.aspx",
        formParameters = formParameters,
    ) {
        headers {
            append(HttpHeaders.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8")
            append(HttpHeaders.AcceptEncoding, "gzip, deflate, br")
            append(HttpHeaders.ContentType, "application/x-www-form-urlencoded")
            append(HttpHeaders.AcceptLanguage, "sv-SE,sv;q=0.8,en-US;q=0.5,en;q=0.3")
        }
    }
    if(response.status.value < 300) {
        throw IllegalArgumentException("Invalid username or password")
    }
}
class TekSystemsHttpClient(
    private val client: HttpClient,
) {
    suspend fun getReportedTime(month: YearMonth): List<ReportedTime> {
        val encryptedDate = LocalDate.of(month.year, month.month, 1).with(TemporalAdjusters.lastInMonth(DayOfWeek.SATURDAY)).toEncryptedValue()

        val times = mutableListOf<ReportedTime>()
        val response = client.get("/webtime/TimeCardHome.aspx?hYXDMK963ND3nmjM=${encryptedDate}") {
            headers {
                append(HttpHeaders.AcceptLanguage, "sv-SE,sv;q=0.8,en-US;q=0.5,en;q=0.3")
            }
            cookie("lang", "sv-SE")
        }
        val elements: ArrayList<Element> = Jsoup.parse(response.body<String>())
            .select("#m_ctl00_f_pnlTotals")
            .select("tbody")
            .select("tr")
        for(element in elements) {
            val styles = Optional.of(element.attr("style"))
                .stream()
                .flatMap { it.split(";").stream() }
                .filter{ !it.isBlank() }
                .map { it.split(":") }
                .collect(Collectors.toMap({ it[0] }, { it[1] }))
            if (!styles.get("display").equals("none")) {
                val durationText = element.lastElementChild()?.text() ?: error("Oh No. Teksystems has changed their API")
                val split = durationText.split(":")

                times.add(ReportedTime(element.firstElementChild()?.text() ?: error("Oh No. Teksystems has changed their API"), Duration.ofMinutes(split[0].toLong() * 60 + split[1].toInt())))
            }
        }
        return times
    }
}

private fun LocalDate.toEncryptedValue(): String {
    return toString()
        .map { char ->
            when (char) {
                '0' -> 'z'
                '1' -> 'w'
                '2' -> 'r'
                '3' -> 'y'
                '4' -> 'i'
                '5' -> 'p'
                '6' -> 's'
                '7' -> 'f'
                '8' -> 'h'
                '9' -> 'k'
                else -> char
            }
        }.joinToString("")
}